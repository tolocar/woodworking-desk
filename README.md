# workbench for woodworking
# description

this is a woodworking bench

# main specs
it is made og solid wood bars and planks
uses 96mm step hole grid for part fixating

## key resources
files with 3d models & pics
manuals






## License
License
Hardware design, CAD and PCB files, BOM, settings and other technical or design files are released under the following license:

CERN Open Hardware Licence Version 2 Weakly Reciprocal - CERN-OHL-W


Assembly manual, pictures, videos, presentations, description text and other type of media are released under the following license:

Creative-Commons-Attribution-ShareAlike 4.0 International - CC BY-SA 4.0

